package com.company;

public class Stars {

    public void firstStars() {

        String[][] starsArray = new String[][]{
                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }

    public void secondStars() {

        String[][] starsArray = new String[][]{
                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},

                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},

                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},

                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},

                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void thirdStars() {

        String[][] starsArray = new String[][]{
                {"   ", "   ", "   ", "   ", "   ", "   ", " * "},

                {"   ", "   ", "   ", "   ", "   ", " * ", " * "},

                {"   ", "   ", "   ", "   ", " * ", "   ", " * "},

                {"   ", "   ", "   ", " * ", "   ", "   ", " * "},

                {"   ", "   ", " * ", "   ", "   ", "   ", " * "},

                {"   ", " * ", "   ", "   ", "   ", "   ", " * "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void fourthStars() {

        String[][] starsArray = new String[][]{
                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {" * ", "   ", "   ", "   ", "   ", " * ", "   "},

                {" * ", "   ", "   ", "   ", " * ", "   ", "   "},

                {" * ", "   ", "   ", " * ", "   ", "   ", "   "},

                {" * ", "   ", " * ", "   ", "   ", "   ", "   "},

                {" * ", " * ", "   ", "   ", "   ", "   ", "   "},

                {" * ", "   ", "   ", "   ", "   ", "   ", "   "},
        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void fifthStars() {

        String[][] starsArray = new String[][]{
                {" * ", "   ", "   ", "   ", "   ", "   ", "   "},

                {" * ", " * ", "   ", "   ", "   ", "   ", "   "},

                {" * ", "   ", " * ", "   ", "   ", "   ", "   "},

                {" * ", "   ", "   ", " * ", "   ", "   ", "   "},

                {" * ", "   ", "   ", "   ", " * ", "   ", "   "},

                {" * ", "   ", "   ", "   ", "   ", " * ", "   "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},
        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void sixthStars() {

        String[][] starsArray = new String[][]{
                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {"   ", " * ", "   ", "   ", "   ", "   ", " * "},

                {"   ", "   ", " * ", "   ", "   ", "   ", " * "},

                {"   ", "   ", "   ", " * ", "   ", "   ", " * "},

                {"   ", "   ", "   ", "   ", " * ", "   ", " * "},

                {"   ", "   ", "   ", "   ", "   ", " * ", " * "},

                {"   ", "   ", "   ", "   ", "   ", "   ", " * "},
        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void seventhStars() {

        String[][] starsArray = new String[][]{
                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},

                {"   ", " * ", "   ", "   ", "   ", " * ", "   "},

                {"   ", "   ", " * ", "   ", " * ", "   ", "   "},

                {"   ", "   ", "   ", " * ", "   ", "   ", "   "},

                {"   ", "   ", " * ", "   ", " * ", "   ", "   "},

                {"   ", " * ", "   ", "   ", "   ", " * ", "   "},

                {" * ", "   ", "   ", "   ", "   ", "   ", " * "},
        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void eighthStars() {

        String[][] starsArray = new String[][]{
                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},

                {"   ", " * ", "   ", "   ", "   ", " * ", "   "},

                {"   ", "   ", " * ", "   ", " * ", "   ", "   "},

                {"   ", "   ", "   ", " * ", "   ", "   ", "   "},

                {"   ", "   ", "   ", "   ", "   ", "   ", "   "},

                {"   ", "   ", "   ", "   ", "   ", "   ", "   "},

                {"   ", "   ", "   ", "   ", "   ", "   ", "   "},
        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
        System.out.println();
    }
    public void ninethStars() {

        String[][] starsArray = new String[][]{
                {"   ", "   ", "   ", "   ", "   ", "   ", "   "},

                {"   ", "   ", "   ", "   ", "   ", "   ", "   "},

                {"   ", "   ", "   ", "   ", "   ", "   ", "   "},

                {"   ", "   ", "   ", " * ", "   ", "   ", "   "},

                {"   ", "   ", " * ", "   ", " * ", "   ", "   "},

                {"   ", " * ", "   ", "   ", "   ", " * ", "   "},

                {" * ", " * ", " * ", " * ", " * ", " * ", " * "},
        };
        for (int i = 0; i <= 6; i++) {
            System.out.println();
            for (int b = 0; b <= 6; b++) {
                System.out.print(starsArray[i][b]);
            }
        }
    }
}
